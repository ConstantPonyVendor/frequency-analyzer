package ru.mirea.security;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.*;

public class Analyzer {

    public static void main(String[] args) {

        String plainFilePath;
        String encryptedFilePath;
        String outputFilePath;

        CommandLine commandLine = null;

        Options options = new Options();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        Option plainFile = new Option("p", "plain-file", true, "not encrypted file path");
        plainFile.setRequired(true);
        options.addOption(plainFile);

        Option encryptedFile = new Option("e", "encrypted-file", true, "encrypted file path");
        encryptedFile.setRequired(true);
        options.addOption(encryptedFile);

        Option output = new Option("o", "output", true, "output file");
        output.setRequired(false);
        options.addOption(output);

        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException exception) {
            formatter.printHelp("frequency-analyzer", options);
            System.exit(1);
        }

        plainFilePath = commandLine.getOptionValue("plain-file");
        encryptedFilePath = commandLine.getOptionValue("encrypted-file");
        outputFilePath = commandLine.getOptionValue("output");

        writeFile(outputFilePath, decrypt(readFile(plainFilePath), readFile(encryptedFilePath)));
    }

    private static String decrypt(String plainText, String encryptedText) {

        Map<Character, Character> comparativeWordMap = getComparativeWordMap(plainText, encryptedText);
        Map<String, String> comparativeBigramMap = getComparativeBigramMap(plainText, encryptedText);

        StringBuilder decryptedText = new StringBuilder();

        encryptedText = encryptedText.toLowerCase();

        for (int i = 0; i < encryptedText.length() - 1; i++) {
            StringBuilder bigram = new StringBuilder() ;
            bigram.append(encryptedText.charAt(i));
            bigram.append(encryptedText.charAt(i + 1));

            if (comparativeBigramMap.containsKey(bigram.toString())) {
                decryptedText.append(comparativeBigramMap.get(bigram.toString()));
                i++;
            } else {
                decryptedText.append(comparativeWordMap.getOrDefault(encryptedText.charAt(i), encryptedText.charAt(i)));
            }
        }

        return decryptedText.toString();
    }

//    private static String wordDecrypt(String plainText, String encryptedText) {
//
//        Map<Character, Character> comparativeMap = getComparativeWordMap(plainText, encryptedText);
//
//        StringBuilder decryptedText = new StringBuilder();
//
//        encryptedText = encryptedText.toLowerCase();
//
//        for (Character character : encryptedText.toCharArray())
//            decryptedText.append(comparativeMap.getOrDefault(character, character));
//
//        return decryptedText.toString();
//    }
//
//    private static String bigramDecrypt(String plainText, String encryptedText) {
//
//        Map<String, String> comparativeMap = getComparativeBigramMap(plainText, encryptedText);
//
//        StringBuilder decryptedText = new StringBuilder();
//
//        encryptedText = encryptedText.toLowerCase();
//
//        for (int i = 0; i < encryptedText.length() - 1; i++) {
//            StringBuilder bigram = new StringBuilder() ;
//            bigram.append(encryptedText.charAt(i));
//            bigram.append(encryptedText.charAt(i + 1));
//
//            if (comparativeMap.containsKey(bigram.toString())) {
//                decryptedText.append(comparativeMap.get(bigram.toString()));
//                i++;
//            } else {
//                decryptedText.append(encryptedText.charAt(i));
//            }
//        }
//
//        return decryptedText.toString();
//    }

    private static Map<Character, Character> getComparativeWordMap(String plainText, String encryptedText) {

        List<Map.Entry<Character, Integer>> plainStat = new ArrayList<>(getWordStat(plainText).entrySet());
        plainStat.sort((a, b) -> b.getValue().compareTo(a.getValue()));

        List<Map.Entry<Character, Integer>> encryptedStat = new ArrayList<>(getWordStat(encryptedText).entrySet());
        encryptedStat.sort((a, b) -> b.getValue().compareTo(a.getValue()));

        Map<Character, Character> comparativeMap = new TreeMap<>();

        for (int i = 0; i < 26; i++) {

            if (encryptedStat.size() <= i)
                return comparativeMap;

            if (plainStat.size() <= i)
                return comparativeMap;

            comparativeMap.put(encryptedStat.get(i).getKey(), plainStat.get(i).getKey());
        }

        return comparativeMap;
    }

    private static Map<String, String> getComparativeBigramMap(String plainText, String encryptedText) {

        List<Map.Entry<String, Integer>> plainStat = new ArrayList<>(getBigramStat(plainText).entrySet());
        plainStat.sort((a, b) -> b.getValue().compareTo(a.getValue()));

        List<Map.Entry<String, Integer>> encryptedStat = new ArrayList<>(getBigramStat(encryptedText).entrySet());
        encryptedStat.sort((a, b) -> b.getValue().compareTo(a.getValue()));

        Map<String, String> comparativeMap = new TreeMap<>();

        for (int i = 0; i < 5; i++) {

            if (encryptedStat.size() <= i)
                return comparativeMap;

            if (plainStat.size() <= i)
                return comparativeMap;

            comparativeMap.put(encryptedStat.get(i).getKey(), plainStat.get(i).getKey());
        }

        return comparativeMap;
    }

    private static Map<Character, Integer> getWordStat(String string) {

        Map<Character, Integer> map = new TreeMap<>();

        for (Character character : string.toLowerCase().toCharArray())
            if (character.toString().matches("[a-zA-Z]"))
                map.put(character, (map.containsKey(character)) ? map.get(character) + 1 : 1);

        return map;
    }

    private static Map<String, Integer> getBigramStat(String string) {

        Map<String, Integer> map = new TreeMap<>();

        StringBuilder bigram = new StringBuilder();

        for (Character character : string.toLowerCase().toCharArray()) {
            bigram.append(character);

            if (bigram.length() > 2)
                bigram.deleteCharAt(0);

            if (bigram.toString().matches("[a-zA-Z][a-zA-Z]"))
                map.put(bigram.toString(), (map.containsKey(bigram.toString())) ? map.get(bigram.toString()) + 1 : 1);

        }

        return map;
    }

    private static String readFile(String fileName) {
        String content = "";

        try {
            content = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (NoSuchFileException e) {
            System.out.println("Error: file \"" + fileName + "\" not found!");

            System.exit(1);
        } catch (IOException e) {
            System.out.println("Error: file read error!");

            System.exit(1);
        }

        return content;
    }

    private static void writeFile(String fileName, String content) {
        try {
            Files.write(Paths.get(fileName), content.getBytes());
        } catch (IOException e) {
            System.out.println("Error: file write error!");

            System.exit(1);
        }
    }
}